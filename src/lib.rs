/// Logger implementation for colors in the terminal and in the future
/// logging to a file and to a remote log server
use colored::*;
use log::{set_logger, set_max_level, Level, LevelFilter, Log, Metadata, Record, SetLoggerError};

pub use log::{debug, error, info, trace, warn};

#[cfg(test)]
mod tests;

static LOGGER: Logger = Logger;

struct Logger;

impl Log for Logger {
	fn enabled(&self, metadata: &Metadata) -> bool {
		metadata.level() <= Level::Trace
	}

	fn log(&self, record: &Record) {
		if self.enabled(record.metadata()) {
			let now = chrono::Local::now();
			let ts = now.format("%Y-%m-%dT%H:%M:%S%.3f%z").to_string();

			let (msg, _level) = match record.level() {
				Level::Error => (
					format!("{} {} {}", ts.white().dimmed(), "ERR".red(), record.args()),
					3,
				),
				Level::Warn => (
					format!("{} {} {}", ts.white().dimmed(), "WRN".purple(), record.args()),
					4,
				),
				Level::Info => (
					format!("{} {} {}", ts.white().dimmed(), "INF".cyan(), record.args()),
					6,
				),
				Level::Debug => (
					format!("{} {} {}", ts.white().dimmed(), "DBG".yellow(), record.args()),
					7,
				),
				Level::Trace => (
					format!("{} {} {}", ts.white().dimmed(), "TRC".green(), record.args()),
					0,
				),
			};
			println!("{}", msg);
		}
	}

	fn flush(&self) {}
}

pub fn init() -> Result<(), SetLoggerError> {
	Ok(set_logger(&LOGGER).map(|()| set_max_level(LevelFilter::Info))?)
}

pub fn set_level_error() {
	set_max_level(LevelFilter::Error);
}

pub fn set_level_warn() {
	set_max_level(LevelFilter::Warn);
}

pub fn set_level_info() {
	set_max_level(LevelFilter::Info);
}

pub fn set_level_debug() {
	set_max_level(LevelFilter::Debug);
}

pub fn set_level_trace() {
	set_max_level(LevelFilter::Trace);
}
